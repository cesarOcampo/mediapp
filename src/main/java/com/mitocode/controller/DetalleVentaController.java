package com.mitocode.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.DetalleVenta;
import com.mitocode.service.IDetalleVentaService;

@RestController
@RequestMapping("/detalle")
public class DetalleVentaController {
    @Autowired
	private IDetalleVentaService service;
    
	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<DetalleVenta>> listar(){
		List<DetalleVenta> detalleVenta = new ArrayList<>();
		try {
			detalleVenta = service.listar();
		}catch(Exception e) {
			return new ResponseEntity<List<DetalleVenta>>(detalleVenta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<DetalleVenta>>(detalleVenta, HttpStatus.OK);
	}
	
	
	@PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DetalleVenta> registrar(@RequestBody DetalleVenta detalleVenta) {
		DetalleVenta pac = new DetalleVenta();
		try {
			pac = service.registar(detalleVenta);			
		} catch (Exception e) {
			return new ResponseEntity<DetalleVenta>(pac, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<DetalleVenta>(pac, HttpStatus.OK);
	}
	

}
