package com.mitocode.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mitocode.dao.IVentaDAO;
import com.mitocode.model.Venta;
import com.mitocode.service.IVentaService;
@Service
public class VentaServiceImpl implements IVentaService {
    @Autowired
	private IVentaDAO dao;

	@Override
	public Venta registar(Venta venta) {
		// TODO Auto-generated method stub
		
		//Esto es java7
		/*for(DetalleConsulta det : consulta.getDetalleConsulta()) {
			det.setConsulta(consulta);
		}*/
		
		//Esto es java8
		venta.getDetalleVenta().forEach(x -> x.setVenta(venta));
		//return dao.save(consulta);
		return dao.save(venta);
	}

	@Override
	public List<Venta> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}


}
