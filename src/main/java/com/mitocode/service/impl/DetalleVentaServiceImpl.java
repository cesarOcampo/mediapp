package com.mitocode.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mitocode.dao.IVentaDAO;
import com.mitocode.dao.IVentaDetalleDAO;
import com.mitocode.model.DetalleVenta;
import com.mitocode.model.Venta;
import com.mitocode.service.IDetalleVentaService;
import com.mitocode.service.IVentaService;
@Service
public class DetalleVentaServiceImpl implements IDetalleVentaService {
    @Autowired
	private IVentaDetalleDAO dao;

	@Override
	public DetalleVenta registar(DetalleVenta detalleVenta) {
		// TODO Auto-generated method stub
		return dao.save(detalleVenta);
	}

	@Override
	public List<DetalleVenta> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}




}
