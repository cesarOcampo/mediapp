package com.mitocode.service;

import java.util.List;

import com.mitocode.model.DetalleVenta;

public interface IDetalleVentaService {
	DetalleVenta registar(DetalleVenta detalleVenta);
	List<DetalleVenta> listar();

}
